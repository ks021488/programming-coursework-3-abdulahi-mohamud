#include <string>
using namespace std;
class Car {
public: // visibility, details taught later // DATA
	int registration;
	string colour;
	// METHODS, i.e., functions on objects
	int getRegistration(); string getColour();
	void setRegistration(int pReg);
	void setColour(string pColour);
};
