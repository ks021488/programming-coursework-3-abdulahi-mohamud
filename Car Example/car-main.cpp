#include <iostream> 
#include <Car.hpp>
using namespace std;
int main() {
	Car car1; // creates a new instance
	car1.setColour("blue");
	Car car2; // creates a new instance
	car2. setColour("red");
	// access to data is possible with the "." punctuator
	cout << "car1 colour is: " << car1.colour << endl;
	cout << "car2 colour is: " << car2.getColour() << endl; return 0;
}
