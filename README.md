# Programming Coursework 3 - Abdulahi Mohamud

Question 1
What is the significance of using OOP? Demonstrate your in depth understanding of polymorphism with an example?

OOP break the problem you are trying to solve into bit-sized, manageable chunks (objects) that can be solved one by one. By using inheritance, we can get rid of redundant or repeating code and use existing classes in their place. An example of polymorphism in the real world is people. People can have many different forms and have different characteristics at any one time. Like a woman who can be a mother, a daughter and a sister all at one time.




